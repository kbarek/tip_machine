//
//  ViewController.m
//  Tip Machine
//
//  Created by Karl Barek on 5/17/13.
//  Copyright (c) 2013 pixels2bytes. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    self.mainView.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    self.mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
   
    self.sliderLabel.text = @"20";
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDecimalNumber *)calculateTip:(int)tipValue{
    NSString *bill = self.billField.text;
    NSDecimalNumber *billAmount = [[NSDecimalNumber alloc] initWithString:bill];
    NSDecimalNumber *tipNumber = [[NSDecimalNumber alloc] initWithInt:tipValue];
    NSDecimalNumber *tipDivisor = [[NSDecimalNumber alloc] initWithDouble:.01];
    NSDecimalNumber *tipDecimal = [tipNumber decimalNumberByMultiplyingBy:tipDivisor];
    NSDecimalNumber *tip = [billAmount decimalNumberByMultiplyingBy:tipDecimal];

    return tip;
}

- (NSDecimalNumber *)calculateBillTotal:(NSDecimalNumber *)bill
                           :(NSDecimalNumber *)tip{
    return [bill decimalNumberByAdding:tip];
}

- (IBAction)textFieldDoneEditing:(id)sender{
    [sender resignFirstResponder];
    
}

- (IBAction)backgroundTap:(id)sender{
    @try {
        //formats decimal number to .xx  for currency
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterDecimalStyle];
        [nf setMaximumFractionDigits:2];
        
        //default 20% caclulations
        NSDecimalNumber *initalTip = [self calculateTip:20];
        NSString *numberString = [nf stringFromNumber:initalTip];
        self.tipLabel.text = numberString;
        self.sliderLabel.text = @"20";
        self.slider.value = 20;
        
        NSString *billString = self.billField.text;
        NSDecimalNumber *billAmount = [[NSDecimalNumber alloc] initWithString:billString];
        NSString *initalTotal = [nf stringFromNumber:[self calculateBillTotal:billAmount :initalTip]];
        self.totalField.text = initalTotal;
        [self.billField resignFirstResponder];
    }
    @catch (NSException *exception) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"You must enter a value." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [errorAlert show];
    }
    @finally {
        [self.billField resignFirstResponder];

    }
    
    
}

- (IBAction)sliderChanged:(UISlider *)sender {
    @try {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterDecimalStyle];
        [nf setMaximumFractionDigits:2];
        
        int progress = lroundf(sender.value);
        self.sliderLabel.text = [NSString stringWithFormat:@"%d", progress];
        NSDecimalNumber *initalTip = [self calculateTip:progress];
        NSString *tipString = [nf stringFromNumber:initalTip];
        self.tipLabel.text = tipString;
        
        NSString *billString = self.billField.text;
        NSDecimalNumber *billAmount = [[NSDecimalNumber alloc] initWithString:billString];
        NSDecimalNumber *tipAmount = [[NSDecimalNumber alloc] initWithString:tipString];
        NSString *billTotal = [[self calculateBillTotal:billAmount :tipAmount] stringValue];
        self.totalField.text = billTotal;
    }
    @catch (NSException *exception) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"You must enter a value." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [errorAlert show];
    }


    
}

@end

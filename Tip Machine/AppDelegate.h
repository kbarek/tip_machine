//
//  AppDelegate.h
//  Tip Machine
//
//  Created by Karl Barek on 5/17/13.
//  Copyright (c) 2013 pixels2bytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
